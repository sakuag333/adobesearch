import math
import Utils as utils
import os

def GetReadStream(filePath):
	with open (filePath, "r") as myfile:
		return myfile.read()

def WriteToFiles(stream, path, numFiles = 1):
	utils.debug("Writing")
	lenStr = len(stream)
	if(numFiles <= 0):
		return
	div = lenStr/numFiles
	if(lenStr % numFiles != 0):
		div += 1
	if(div == 0):
		return
	cnt = 0
	cntFiles = 0
	while(cnt < lenStr):
		fileName = path + "/" + str(cnt/div) + ".txt"			## Use function pointer for getting file names
		file = open(fileName, "w+")
		cntFiles += 1
		for i in range(0, div):
			if(cnt >= lenStr):
				break
			#utils.debug(stream[cnt])
			print>>file, stream[cnt]
			cnt += 1
		file.close()
		if(cntFiles % 100 == 0):
			utils.debug((cntFiles, numFiles))
	return cntFiles


## All words in a file are in 0 based position
## name of files are 0 based
def SearchInFiles(filesHashPath, wordsHashPath, word):
	stream = GetReadStream(filesHashPath)
	stream = stream.split()
	#utils.debug(stream)
	filePos = utils.BinarySearch(stream, word, fuzzyMode = True)
	if(filePos < 0):
		return -1
	listPos = []
	currFile = filePos
	#utils.debug(wordsHashPath)
	totalFiles = len(os.listdir(wordsHashPath))
	#utils.debug(totalFiles)
	while(currFile < totalFiles):
		fileName = wordsHashPath + "/" +str(currFile) + ".txt"
		#utils.debug(fileName)
		words = GetReadStream(fileName)
		words = words.split()
		start = utils.GetStartsWithLeft(words, word)
		nextWord = utils.GetNextStringLC(word)
		#utils.debug(nextWord)
		end = utils.GetStartsWithRight(words, nextWord)
		if(start >= 0 and end >= start):
			listPos.append((start, end))
		if(end < len(words) - 1):
			break
		currFile += 1
	return (filePos, listPos)

##lineNum is zero based
def GetNthLine(filePath, lineNum):
	with open(filePath) as infile:
	    for line in infile:
	    	if(lineNum == 0):
	    		return line
	    	lineNum -= 1
	return -1

## start and end are zero based
## returns line in [start, end)
##	appends lines in a string
## improve this function, should not return as string, but a list
def GetLinesInRange(filePath, start, end):
	#utils.debug((start, end))
	lines = ""
	if(start < 0 or end <= 0 or start >= end):
		return lines
	with open(filePath) as infile:
		for line in infile:
			if(end == 0):
				break
			if(start == 0):
				lines += line
			else:
				start -= 1
			end -= 1
	return lines

## returns all lines inside a file as a list
## if getlinbeinrange() gets improved, no need for this function
def GetFileAsLineList(filePath):
	lines = []
	with open(filePath) as infile:
		for line in infile:
				lines.append(line)
	return lines