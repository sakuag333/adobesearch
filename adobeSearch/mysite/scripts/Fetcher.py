import Utils as utils
import FileUtils as fl
import os

class FetcherCore:
	def __init__(self):
		utils.debug()

	
	def __del__(self):
		pass


	def QueryMatchingWordList(self, word):
		word = word.lower()
		basePath = os.path.dirname(os.path.abspath(__file__)).replace('\\','/')
		result = fl.SearchInFiles(basePath + "/FilesHash/0.txt", basePath + "/WordsHash", word)		## Make 'result' OO by making it an object 
		startFile = result[0]
		wordIndexList = result[1]
		numFiles = len(wordIndexList)
		matchWords = []
		#utils.debug(wordIndexList)
		#utils.debug(numFiles)
		for i in range(0, numFiles):
			filePath = basePath + "/WordsHash/" + str(startFile + i) + ".txt"
			words = fl.GetLinesInRange(filePath, wordIndexList[i][0], wordIndexList[i][1])
			words = words.split("\n")
			matchWords += words[0 : len(words) -1]
			
		"""
		for w in matchWords:
			utils.debug(w)
		"""
		
		return matchWords


	def QueryFilesList(self, exactWord):
		basePath = os.path.dirname(os.path.abspath(__file__)).replace('\\','/')
		exactWord = exactWord.lower()
		result = fl.SearchInFiles(basePath + "/FilesHash/0.txt", basePath + "/WordsHash", exactWord)		## Make 'result' OO by making it an object 
		fileIndex = result[0]
		line = result[1][0][0]
		filePath = basePath + "/FilesListHash/" + str(fileIndex) + ".txt"
		filesList = fl.GetNthLine(filePath, line)
		#utils.debug(filesList)
		#filesList = filesList.rstrip()
		#utils.debug(filesList)
		filesList = filesList.split("$")
		filesList.pop(len(filesList) - 1)
		filesList = sorted(filesList)
		filePathList = []
		lines = fl.GetFileAsLineList(basePath + "/FilesPathHash/0.txt")
		for f in filesList:
			#print int(f)
			filePathList.append(lines[int(f)].split("\n")[0])

		#utils.debug(filesList)
		#utils.debug(fileIndex)
		#utils.debug(line)
		#utils.debug(len(filesList))
		"""
		utils.debug("***********")
		for k in filesList:
			utils.debug(k)
			#utils.debug("======")
		utils.debug("***********")
		"""
		return filePathList
			



if __name__ == '__main__':
	fetcher = FetcherCore()
	#fetcher.QueryMatchingWordList("GETNt")
	files = fetcher.QueryFilesList("indexer")
	utils.debug(files)