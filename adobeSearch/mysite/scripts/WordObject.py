class WordObj:
	fID = None
	fWord = None
	fFilesList = []

	def __init__(self, id, word):
		self.fID = id
		self.fWord = word
		self.fFilesList = []

	def InsertFile(self, filePath):
		self.fFilesList.append(filePath)

	def GetWord(self):
		return self.fWord