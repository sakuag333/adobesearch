import Utils as utils
import FileUtils as fl
from WordObject import WordObj as obj
import os
import shutil

class IndexerCore:
	fFolderList = None
	fWordMap = None
	fCounter = None
	fFilesList = None
	fNumFiles = None
	fSuccess = None
	fFileMap = None

	def __init__(self, pathList, numFiles = 10, doIndex = True):
		utils.debug(pathList)
		self.fSuccess = False
		self.fFolderList = pathList
		self.fWordMap = {}
		self.fCounter = 0
		self.fFilesList = []
		self.fFileMap = {}
		if(doIndex):
			cnt = 0
			for folder in self.fFolderList:
				if(folder == ""):
					continue
				tmp = utils.GetAllFilesInside(folder)
				self.fFilesList += tmp
				for t in tmp:
					self.fFileMap[t] = cnt
					cnt += 1
				utils.debug(folder)
			self.fNumFiles = len(self.fFilesList)/20
			self.CreateIndex()
		self.fSuccess = True

		#self.fNumFiles = numFiles

	def __del__(self):
		del self.fFolderList
		del self.fWordMap
		del self.fCounter
		del self.fFilesList

	def UpdateIndex(self, word, filePath):
		if(word not in self.fWordMap ):
			self.fWordMap[word] = obj(self.fCounter, word)
			self.UpdateCounter()
			#utils.debug(fCounter)
		self.fWordMap[word].InsertFile(filePath)

	def IndexFile(self, filePath):
		textStream = fl.GetReadStream(filePath)
		wordsList = utils.GetWordsFromStream(textStream)
		wordsList = set(wordsList)
		for word in wordsList:
			self.UpdateIndex(word, filePath)


	def CreateIndex(self):
		utils.debug("Create Index Called")
				## let the ball roll
		cnt = 0
		lent = len(self.fFilesList)
		for file in self.fFilesList:
			self.IndexFile(file)
			cnt += 1
			if(cnt % 1000 == 0):
				utils.debug((cnt, lent))
			#utils.debug(file)
		self.CreateDir()
		self.SaveIndex()
		#utils.debug()
		utils.debug("Create Index End")

	def UpdateCounter(self):
		self.fCounter += 1

	def PrintSortedWords(self, lim = 10):
		words = []
		for k in self.fWordMap:
			words.append(self.fWordMap[k].GetWord())
			#pass
		sortedList = sorted(words)
		if(lim > len(sortedList)):
			lim = len(sortedList)
		for i in range(0, lim):
			print sortedList[i]

	def SaveIndex(self):
		utils.debug("Save Index Called")
		#utils.debug(self.fNumFiles)
		basePath = os.path.dirname(os.path.abspath(__file__)).replace('\\','/')
		utils.debug(basePath)
		words = []
		for k in self.fWordMap:
			words.append(k)
		utils.debug(len(words))
		sortedList = sorted(words)
		folderPath = basePath + "/WordsHash"
		#self.numFiles = 10
		self.fNumFiles = fl.WriteToFiles(sortedList, folderPath, self.fNumFiles)
		startWords = []
		for i in range(0, self.fNumFiles):
			filePath = folderPath + "/" + str(i) + ".txt"
			stream = fl.GetNthLine(filePath, 0)
			stream =stream.split()
			startWords.append(stream[0])
		#print startWords
		fl.WriteToFiles(startWords, basePath + "/FilesHash", numFiles = 1)

		misc = [self.fNumFiles]
		fl.WriteToFiles(misc, basePath + "/MiscData", numFiles = 1)

		fl.WriteToFiles(self.fFilesList, basePath + "/FilesPathHash", numFiles = 1)

		fileListCollection = []
		for i in range(0, len(sortedList)):
			filesList = self.fWordMap[sortedList[i]].fFilesList
			largeStr = ""
			for f in filesList:
				largeStr += str(self.fFileMap[f])
				largeStr += "$"
			fileListCollection.append(largeStr)
		fl.WriteToFiles(fileListCollection, basePath + "/FilesListHash", numFiles = self.fNumFiles)
		utils.debug("Save Index End")

	def CreateDir(self):
		utils.debug("Create Dir Called")
		basePath = os.path.dirname(os.path.abspath(__file__)).replace('\\','/')
		if os.path.exists(basePath + "/FilesHash"):
			shutil.rmtree(basePath + "/FilesHash")
			utils.debug(basePath)
		if os.path.exists(basePath + "/MiscData"):
			shutil.rmtree(basePath + "/MiscData")
		if os.path.exists(basePath + "/FilesListHash"):
			shutil.rmtree(basePath + "/FilesListHash")
		if os.path.exists(basePath + "/WordsHash"):
			shutil.rmtree(basePath + "/WordsHash")
		if os.path.exists(basePath + "/FilesPathHash"):
			shutil.rmtree(basePath + "/FilesPathHash")

		os.makedirs(basePath + "/FilesHash")
		os.makedirs(basePath + "/MiscData")
		os.makedirs(basePath + "/FilesListHash")
		os.makedirs(basePath + "/WordsHash")
		os.makedirs(basePath + "/FilesPathHash")

		utils.debug("Create Dir End")




if __name__ == '__main__':
	listd = ["C:/Users/Sandagra/adobeSearch"]
	indexer = IndexerCore(listd)
	#indexer.CreateIndex()
	#indexer.PrintSortedWords(2)
	pass