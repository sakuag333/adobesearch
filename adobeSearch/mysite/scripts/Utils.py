import os
from os.path import isdir
from bisect import bisect_left, bisect_right

def GetAllFilesInside(folderPath):
	contents =  os.listdir(folderPath)
	files = []
	for item in contents:
		path = folderPath + "/" + item
		if isdir(path):
			files += GetAllFilesInside(path)
		else:
			files.append(path)
	return files



def debug(val = None):
	if(val):
		print val
	else:
		print "Hit"

def GetWordsFromStream(textStream):
	word = ""
	lowerWords = []
	text = textStream.lower()
	for i in range(0,len(text)):
		alpha = text[i]
		if(alpha.isalpha()):
			word += alpha
		elif(word != ""):
			lowerWords.append(word)
			word = ""
	return lowerWords

def BinarySearch(array, ele, fuzzyMode = False):
	low = 0
	high = len(array)
	mid = (low + high)/2
	while(low <= high):
		#print low,high
		if(low == high or (fuzzyMode and high - low == 1) ):
			return low
		if(ele < array[mid]):
			high = mid
			if(not(fuzzyMode)):
				high -= 1
		elif(ele > array[mid]):
			low = mid
			if(not(fuzzyMode)):
				low += 1
		else:
			return mid
		mid = (low + high)/2
	return -1

def GetStartsWithLeft(wordList, wordFrag):
	#return any(w.startswith(wordFrag) for w in wordList)
	return bisect_left(wordList, wordFrag)

def GetStartsWithRight(wordList, wordFrag):
	#return any(w.startswith(wordFrag) for w in wordList)
	return bisect_right(wordList, wordFrag)

def GetNextCharacter(char):
	return chr(ord(char) + 1)

def AssignNthPosOfString(str, n, char):
	if(n >= len(str)):
		return str
	listStr = list(str)
	listStr[n] = char
	return "".join(listStr)

# for lowercase strings only
def GetNextStringLC(string):
	lent = len(string)
	curr = lent - 1
	while(curr >= 0):
		if(string[curr] != 'z'):
			string = AssignNthPosOfString(string, curr, GetNextCharacter(string[curr]))
			for i in range(curr + 1 , lent):
				string = AssignNthPosOfString(string, i , 'a')
			return string
		curr -= 1
	str = "z"
	for i in range(0, lent):
		str += "a"
	return str



