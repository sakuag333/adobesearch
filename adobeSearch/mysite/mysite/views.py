from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render
import os
from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from django.template import RequestContext

import sys
absPath = os.path.abspath("./scripts")
sys.path.insert(0, absPath)

from Fetcher import FetcherCore as fc
from FileUtils import GetFileAsLineList as stream
from FileUtils import GetReadStream as stream2
from Indexer import IndexerCore as ic

def hello(request):
	t = get_template('hello.html')
	html = t.render(Context({}))
	return HttpResponse(html)

def search(request):
	t = get_template('search.html')
	html = t.render(Context({}))
	return HttpResponse(html)

def get_match(request):
	term = request.GET['term']
	if(len(term) < 2):
		return []
	fetcher = fc()
	list = fetcher.QueryMatchingWordList(term)
	#list = fetcher.QueryFilesList(term)
	t = get_template('match.html')
	c = Context({'item_list': list[0:100]})
	return HttpResponse(t.render(c))

def get_files(request):
	term = request.GET['term']
	if(term == ""):
		return []
	fetcher = fc()
	list = fetcher.QueryFilesList(term)
	t = get_template('files.html')
	c = Context({'item_list': list})
	return HttpResponse(t.render(c))

def get_content(request):
	text = stream2(request.GET['term'])
	#text = text.replace();
	#t = get_template('content.html')
	#c = Context({'item_list': text})
	return HttpResponse(text)

def make_index(request):
	paths = request.GET['term']
	result = "0"
	listPaths = paths.split(";")
	validPaths = []
	for path in listPaths:
		if path == "" or not(os.path.exists(path)):
			continue
		validPaths.append(path)
	if(len(validPaths) > 0):
		validPaths = set(validPaths)
		Indexer = ic(validPaths)
		if(ic.fSuccess):
			result = "1"
	print "============================"
	print "Completed!!!!!!!!!!!!!!!!!!!!!!"
	print "============================"
	return HttpResponse(result)
	
