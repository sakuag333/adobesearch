from django.conf.urls import patterns, include, url
from mysite.views import hello, search, get_match, get_files, get_content, make_index

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^hello/$', hello),
	url(r'^$', search),
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^jquery_words/$', get_match),
    url(r'^jquery_files/$', get_files),
    url(r'^jquery_content/$', get_content),
    url(r'^jquery_index/$', make_index),
    url(r'^admin/', include(admin.site.urls)),
)
